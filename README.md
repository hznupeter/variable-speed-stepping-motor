# 调速步进电机

#### 介绍
步进电机调速台是基于42步进电机设计的一款可调速转台。通过旋钮调节转台速度，使用简单。

#### 最终成品
![Alt](hardware/2.jpg)

#### 所需硬件
* 12V电源
* 5V电源
* cnc shield V3扩展板
* arduino UNO R3开发板
* 42步进电机
* 圆形转台平面
* 旋钮传感器


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


