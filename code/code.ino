#define EN        8       //步进电机使能端，低电平有效
#define X_DIR     5       //X轴 步进电机方向控制
#define X_STP     2       //x轴 步进控制
#define Y_DIR     6       //y轴 步进电机方向控制
#define Z_DIR     7       //z轴 步进电机方向控制
#define Y_STP     3       //y轴 步进控制
#define Z_STP     4       //z轴 步进控制
#define SPEED_PIN A4
int speed;
void setup() { //将步进电机用到的IO管脚设置成输出
  pinMode(X_DIR, OUTPUT);
  pinMode(X_STP, OUTPUT);
pinMode(Y_DIR, OUTPUT);
  pinMode(Y_STP, OUTPUT);
  pinMode(EN, OUTPUT);
  digitalWrite(EN, LOW);
}

void loop() {
 // speed=map(analogRead(SPEED_PIN), 0, 1023, 20, 5000);
  speed=1500;
  digitalWrite(X_DIR, true);
  digitalWrite(X_STP, HIGH);
 digitalWrite(Y_DIR, true);
  digitalWrite(Y_STP, HIGH);
  delayMicroseconds(speed);
  digitalWrite(X_STP, LOW);
  digitalWrite(Y_STP, LOW);
  delayMicroseconds(speed);
}
